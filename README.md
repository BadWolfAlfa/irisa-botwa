
# Info
Este BOT esta siendo creado para los recursos de Revolution Technologies. 
Su uso sera recreativo aparte de dar soporte automatico a los usuarios de Revolution ROM

## Funciones actuales:
+ **`!ayuda/!help`**: Muestra esta pantalla
+ **`!hola`**: Saluda al usuario
+ **`!yt`**: Activa la deteccion de YouTube
+ **`!siono`**: Random yes or no function. Returns gif and answer from [**yesno.wtf/api**](https://yesno.wtf/api/)
+ **`!poll <title>, <identifier (optional)>`**: Make polls (taking chat as input)
+ **`!poll2 <title>, <cantidates>...`**: Make polls (taking chat as input)
+ **`!<message>`**: Respondera al usuario por medio de Inteligencia Artificial (inestable)
+ **`!info`**: Informacion de version y fecha de version
+ **`!revolution`**: Muestra informacion de Revolution ROM, asi como su actual version en sus diferentes plataformas
+ **`!elo <game>`**: Retrieves rankings of the game
+ **`!match <game>, <results>`**: Records a match to the specified game

## Usar el BOT.
El numero del bot es: *No DISPONIBLE ACTUALMENTE*
 
